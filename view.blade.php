<!-- Start Features 04 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Repeater of text fields with an icon -->
@endif
<div class="features-04" is="fir-features-04" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="features-04__wrap">
      @for ($i = 0; $i < 4; $i++)
      <div class="features-04__panel">
          <span class="features-04__icon">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 48 44">
                <path fill="#333" fill-rule="evenodd" d="M0 24C0 10.746 10.746 0 24 0s24 10.746 24 24c0 5.166-1.632 10.084-4.41 14h-8.714c-1.86 3.294-6.03 6-10.876 6-4.846 0-9.016-2.706-10.876-6H4.41C1.632 34.084 0 29.166 0 24zM24 4c11.028 0 20 8.972 20 20 0 3.516-.916 7-2.606 10H32.54c-.82 2.094-3.29 6-8.54 6-5.312 0-7.718-3.906-8.54-6H6.606C4.916 31 4 27.516 4 24 4 12.972 12.972 4 24 4zm1.34 3.656a16.829 16.829 0 00-1.296-.052c-.426 0-.864.018-1.3.052v5.568a11.238 11.238 0 012.596 0V7.656zm6.38 1.858c-.774-.41-1.58-.758-2.4-1.038l-2.046 5.146c.836.25 1.64.598 2.4 1.038l2.046-5.146zm-13.35 5.17c.76-.442 1.562-.792 2.396-1.046L18.7 8.5c-.824.284-1.626.634-2.4 1.048l2.07 5.136zm-1.592 1.114a11.973 11.973 0 00-1.836 1.914l-3.782-3.936a17.332 17.332 0 011.838-1.914l3.78 3.936zm22.276 1.238c.404.796.75 1.63 1.032 2.484l-4.906 2.212a12.34 12.34 0 00-1.034-2.484l4.908-2.212zm-25.206 2.378c-.422.792-.754 1.63-.996 2.5l-4.94-2.132a18.18 18.18 0 01.996-2.5l4.94 2.132zm21.238-7.552c.656.586 1.274 1.23 1.836 1.914l-3.78 3.936a11.982 11.982 0 00-1.838-1.914l3.782-3.936zm-15.73 18.672a4.687 4.687 0 009.374 0 4.679 4.679 0 00-2.422-4.102l-2.266-10.618-2.266 10.618a4.685 4.685 0 00-2.42 4.102z" clip-rule="evenodd"/>
              </svg>
          </span>
        <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto reprehenderit molestiae quia, ipsa et amet ea repudiandae voluptatum voluptate obcaecati? Voluptatibus vero suscipit alias ex maiores expedita natus esse exercitationem!
        </p>
      </div>
      @endfor
  </div>
</div>
<!-- End Features 04 -->