<?php

namespace Fir\Pinecones\Features04;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Features04',
            'label' => 'Pinecone: Features 04',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Repeater of text fields with an icon"
                ],
                [
                    'label' => 'Features',
                    'name' => 'linksTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Feature',
                    'name' => 'feature',
                    'type' => 'repeater',
                    'layout' => 'table',
                    'min' => 1,
                    'max' => 3,
                    'button_label' => 'Add Feature',
                    'sub_fields' => [
                        [
                            'label' => 'Icon',
                            'name' => 'icon',
                            'type' => 'image'
                        ],
                        [
                            'label' => 'Text',
                            'name' => 'text',
                            'type' => 'textarea'
                        ],
                    ]
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
