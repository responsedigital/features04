class Features04 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initFeatures04()
    }

    initFeatures04 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Features 04")
    }

}

window.customElements.define('fir-features-04', Features04, { extends: 'div' })
